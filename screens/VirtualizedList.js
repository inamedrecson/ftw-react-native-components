import React, { Component } from 'react';
import { VirtualizedList,  StyleSheet, Text, View, Button } from 'react-native';


export default class VirtualL extends React.Component{


			render(){

				return(

						<View>
							<VirtualizedList 
 							 data={[{key: 'a'}, 
 							 		{key: 'b'}, 
 							 		{key: 'c'}, 
 							 		{key: 'd'}, 
 							 		{key: 'e'},
 							 		{key: 'f'},
 							 		{key: 'g'},
 							 		{key: 'h'},
 							 		{key: 'i'},
 							 		{key: 'j'}
 							 	    ]
 							 	} 
 							 renderItem={({item}) => <Text>{item.key}</Text>}
							/>
						</View>

					)

			}

}