import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, ViewPagerAndroid} from 'react-native';


export default class ViewPager extends Component {

      render(){
                
                return (
                  <ViewPagerAndroid
                    style={styles.viewPager}
                    initialPage={0}>
                    <View style={styles.viewPager} key="1">
                      <Text>First page</Text>
                    </View>
                    <View style={styles.pageStyle} key="2">
                      <Text>Second page</Text>
                    </View>
                  </ViewPagerAndroid>
                )
              }

}



var styles = {

  viewPager: {
    flex: 1,
    backgroundColor: "#00ffff"
  },
  pageStyle: {
    alignItems: 'center',
    padding: 20,
    backgroundColor: "#841584"
  }
}