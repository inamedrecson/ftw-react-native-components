import React, { Component } from 'react';
import { TouchableNativeFeedback, StyleSheet, Text, View } from 'react-native';


export default class TouchableNativeFeedbackComp extends Component {

	render() {

		  return(
		  	<TouchableNativeFeedback
		        onPress={this._onPressButton}
		        background={TouchableNativeFeedback.SelectableBackground()}>
		      <View style={{width: 150, height: 100, backgroundColor: 'orange'}}>
		        <Text style={{margin: 30}}>Button</Text>
		      </View>
		    </TouchableNativeFeedback>
		  )
		}


}

