import React, { Component } from 'react';
import { View, Image, StyleSheet, KeyboardAvoidingView, TextInput } from 'react-native';


export default class KbdAvoidingView extends Component {
  render() {
    return (
      
          
          <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <View>

          <TextInput placeholder="Input here" style={{ height: 30}} />

            </View>
             
          </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      justifyContent: 'center',
      padding: 10
    },
  });
