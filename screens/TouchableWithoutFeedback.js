import React, { Component } from 'react'
import { TouchableWithoutFeedback, Text} from 'react-native'

export default class TWFComp extends Component {
	
	render(){

		return (

			<TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('TouchableOpacity')}>
		   <Text>
		      Button
		   </Text>
		</TouchableWithoutFeedback>

		)
	}

}

