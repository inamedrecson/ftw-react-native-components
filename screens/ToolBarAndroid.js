import React, {Component} from 'react';
import {ToolbarAndroid, StyleSheet} from 'react-native';



export default class ToolbarAndroidComp extends Component {
	render() {
  return (
     <ToolbarAndroid
            style={styles.toolbar}
            title="ToolbarAndroid"
            onActionSelected={this.onActionSelected}
            titleColor= "#adff2f"
            actions = {[
              {title: "Log out", show: "never"}
            ]}
            />
  )
}
 onActionSelected(position) {
     }
}

const styles= StyleSheet.create ({

	 toolbar: {
   backgroundColor: '#2196F3',
   height: 90,
   alignSelf: 'stretch',
 }, 
})