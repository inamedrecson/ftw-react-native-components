import React, { Component } from 'react';
import { Alert, StyleSheet, Text, ScrollView, Button, TextInput } from 'react-native';


class HomeScreen extends React.Component {
    render() {
        return (


          <ScrollView>

           <Button title="Activity Indicator" onPress={() => this.props.navigation.navigate('ActInd')} />
           <Button onPress={() => { Alert.alert('You tapped the button!'); }} title="Button" color="#841584" accessibilityLabel="Learn more about this purple button"/>
          <Button title="Drawer Layout" onPress={() => this.props.navigation.navigate('DrawLayout')} color="#00ffff"/>
          <Button title="FLatList" onPress={() => this.props.navigation.navigate('FlatLizt')} color="#00ff00"/>
          <Button title="Image" onPress={() => this.props.navigation.navigate('DispImg')}/>
          <Button title="ImageBackground" onPress={() => this.props.navigation.navigate('DispImgBkg')} color="#00ffff"/>
          <Button title="KeyBoardAvoidingView" onPress={() => this.props.navigation.navigate('KeyAvView')}/>
          <Button title="ListView" onPress={() => this.props.navigation.navigate('LView')} color="#00ffff" />
          <Button title="Modal" onPress={() => this.props.navigation.navigate('Modal')} />
          <Button title="Picker" onPress={() => this.props.navigation.navigate('Picker')} color="#841584" />
          <Button title= "Progress Bar" onPress={() => this.props.navigation.navigate('ProgressBar')}/>
          <Button title= "Refresh Control" onPress={() => this.props.navigation.navigate('RefreshControl')} color="#841584"/>
          <Button title= "ScrollView" onPress={() => this.props.navigation.navigate('ScrollView')}/>
          <Button title= "Section List" onPress={() => this.props.navigation.navigate('SectionList')} color="#841584"/>
          <Button title= "Slider" onPress={() => this.props.navigation.navigate('Slider')}/>
          <Button title= "Status Bar" onPress={() => this.props.navigation.navigate('StatusBar')} color="#841584"/>
          <Button title= "Switch" onPress={() => this.props.navigation.navigate('Switch')}/>
           <Button title="Text" onPress={() => this.props.navigation.navigate('Text')} color="#00ff00"/>
           <Button title= "Text Input" onPress={() => this.props.navigation.navigate('TextInput')}/>
           <Button title="ToolBarAndroid" onPress={() => this.props.navigation.navigate('ToolBarAndroid')} color="#00ff00"/>
           <Button title= "Touchable Highlight" onPress={() => this.props.navigation.navigate('Touchable')}/>
           <Button title="TouchableNativeFeedBack" onPress={() => this.props.navigation.navigate('TouchableNativeFeedBack')} color="#00ff00"/>
            <Button title= "TouchableOpacity" onPress={() => this.props.navigation.navigate('TouchableOpacity')} color="#841584"/>
             <Button title= "TouchableWithoutFeedback" onPress={() => this.props.navigation.navigate('TWF')} color="#00ff00"/>
            <Button title="View" onPress={() => this.props.navigation.navigate('View')} color="#00ffff" />
            <Button title="ViewPager" onPress={() => this.props.navigation.navigate('ViewPager')} />
            <Button title="WebView" onPress={() => this.props.navigation.navigate('WebView')} />
          </ScrollView>

        );
    }
}



export default HomeScreen;
