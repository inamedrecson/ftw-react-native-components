import React, { Component } from 'react';
import { View, ImageBackground, StyleSheet, Text } from 'react-native';



export default class DisplayImgBkg extends Component {
  render() {
    return (
      <View>
       <ImageBackground source={require('../assets/1.png')} style={{width: '100%', height: '100%'}}>
          <Text>Inside</Text>
       </ImageBackground>
      </View>
    );
  }
}