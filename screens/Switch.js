import React, { Component } from 'react'
import { View, Switch, StyleSheet } from 'react-native'


export default class SwitchExample extends Component {

   state = {

      switchValue: true    
   };

   _handleToggleSwitch = () => this.setState(state=> ({ switchValue: !state.switchValue}));

   render(){

      return (
        
         <View style={styles.container}>

            <Switch 
               onValueChange={this._handleToggleSwitch} value={this.state.switchValue}

            />

         </View>
        )
   }

 }

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      alignItems: 'center',
      marginTop: 100
   }
})







