import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import ActivityInd from './screens/ActivityIndicator'
import DrawLay from './screens/DrawerLayoutAndroid'
import FlatL from './screens/FlatList'
import DisplayAnImage from './screens/Image'
import DisplayImgBkg from './screens/ImageBackground'
import KbdAvoidingView from './screens/KeyboardAvoidingView'
import ListV  from './screens/ListView'
import ModalExample from './screens/Modal'
import PickerComp from './screens/Picker'
import ProgBar from './screens/ProgressBarAndroid'
import RefreshableListComp from './screens/RefreshControl'
import ScrollViewComp  from './screens/ScrollView'
import SectionListComp from './screens/SectionList'
import SliderComp from './screens/Slider'
import StatusBarComp from './screens/Statusbar'
import SwitchExample from './screens/Switch'
import TextComp from './screens/Text'
import TextInputComp from './screens/TextInput'
import Touch from './screens/TouchableHighlight'
import ToolBarAndroidComp from './screens/ToolBarAndroid'
import TouchableNativeFeedBackComp from './screens/TouchableNativeFeedBack'
import TouchableOpacityComp from './screens/TouchableOpacity'
import TWFComp from './screens/TouchableWithoutFeedback'
import ViewColoredBoxesWithText from './screens/View'
import ViewPager from './screens/ViewPagerAndroid'
import WebViewComp from './screens/WebView'
import VirtualL from './screens/VirtualizedList'

export default class App extends React.Component {
  render() {

    return <RootStack/>;
  }
}

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    ActInd: ActivityInd,
    DrawLayout: DrawLay,
    FlatLizt: FlatL,
    DispImg: DisplayAnImage,
    DispImgBkg: DisplayImgBkg,
    KeyAvView: KbdAvoidingView,
    LView: ListV,
    Modal: ModalExample,
    Picker: PickerComp,
    ProgressBar: ProgBar,
    RefreshControl: RefreshableListComp,
    ScrollView: ScrollViewComp,
    SectionList: SectionListComp,
    Slider: SliderComp,
    StatusBar: StatusBarComp,
    Switch: SwitchExample,
    Text: TextComp,
    TextInput: TextInputComp,
    Touchable: Touch,
    ToolBarAndroid: ToolBarAndroidComp,
    TouchableNativeFeedBack: TouchableNativeFeedBackComp,
    TouchableOpacity: TouchableOpacityComp,
    TWF: TWFComp,
    View: ViewColoredBoxesWithText,
    ViewPager: ViewPager,
    WebView: WebViewComp,
    VirtualL: VirtualL
  
  },
  {
    initialRouteName: 'Home'
  }  
);
